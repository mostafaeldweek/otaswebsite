import Vue from 'vue'
import Vuelidate from 'vuelidate'
import vuelidateErrorExtractor, { templates } from 'vuelidate-error-extractor'
import FormGroup from '@/components/FormGroup'
const messages = {
  en: require('@/plugins/i18n/en.json'),
  ar: require('@/plugins/i18n/ar.json')
}
Vue.use(Vuelidate)
Vue.use(vuelidateErrorExtractor, {
  messages, i18n: 'validation'
})
Vue.component('FormGroup', FormGroup)
Vue.component('FormWrapper', templates.FormWrapper)
