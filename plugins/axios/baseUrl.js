const getBaseUrl = () => {
  if (process.env.NODE_ENV === "production") {
    return process.env.NUXT_APP_WEBSITE_PRODUCTION;
  } else if (process.env.NODE_ENV === "testing") {
    return process.env.NUXT_APP_WEBSITE_TEST;
  } else {
    return process.env.NUXT_APP_WEBSITE_DEV;
  }
};

export default getBaseUrl;