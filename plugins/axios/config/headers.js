export default (store) => (config) => {
    const locale = store.getters["locale/getLocale"];
    config.headers.common["X-locale"] = locale;
    config.headers.common["Accept-Language"] = locale;
    config.headers.common.Timezone = `UTC +${-new Date().getTimezoneOffset() / 60}`;
  
    const currency = store.getters["currencies/getCurrency"];
    if (currency) {
      config.headers.common["X-currency"] = currency.code || "USD";
    }
  
    return config;
  };
  