import Vue from "vue";
export default (store) => (response) => {
  const { method } = response.config;
  if (["post", "put", "delete"].includes(method)) {
    Vue.toasted.success(response.data.message);
  }
  if (store.getters["errors/serverErrors"] !== null) {
    store.dispatch("errors/ClearServerErrors");
  }
  return response;
};
