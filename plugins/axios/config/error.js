import Vue from "vue";
export default (store, redirect, localePath) => (err) => {
  const { response } = err;
  if (!response) return;
  const { status, data } = response;
  if ([403, 503, 409, 422].includes(status)) {
    Vue.toasted.error(data.message);
  } else if (status == 404) {
    redirect(localePath('/not-found'));
  } else if (data && data.errors) {
    store.dispatch("errors/SetServerErrors", data.errors);
  }
};
