import setCommonHeaders from './config/headers';
import handleResponse from './config/success';
import handleError from './config/error';
export default ({ store, redirect, app: { $axios, localePath } }) => {
  $axios.onRequest(setCommonHeaders(store));
  $axios.onResponse(handleResponse(store));
  $axios.onError(handleError(store, redirect, localePath));
};
