import Vue from "vue";
import { mapGetters } from "vuex";
Vue.mixin({
  computed: {
    ...mapGetters({
      serverErrors: "errors/serverErrors",
      settings: "globalSettings/getSettings",
    }),

  },
  methods: {
    pmAmDate(date) {
      const inputDate = new Date(date);
      const options = {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
        hour12: true,
      };
      return inputDate.toLocaleDateString(this.$i18n.locale, options);
    },
  },
});
