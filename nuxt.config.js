import theme from "./utils/theme";
import baseUrl  from "./plugins/axios/baseUrl";
export default {
  head: {
    titleTemplate: "%s",
    title: "Otas",
    meta: [
      { charset: "utf-8" },
      {
        name: "viewport",
        content: "width=device-width, initial-scale=1",
      },
      { hid: "description", name: "description", content: "Otas Website Travel" },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.svg' },
    ],
  },
  css: [
    "@/assets/scss/main.scss",
    "@/assets/fonts/main.scss",
  ],
  pageTransition: {
    name: "fade",
    mode: "out-in",
  },
  loadingIndicator: {
    name: "cube-grid",
    color: "#f00",
    background: "white",
  },
  components: true,
  buildModules: [
    "@nuxtjs/vuetify",
    "@nuxtjs/moment",
  ],
  moment: {
    defaultLocale: "en",
    locales: ["ar"],
  },
  modules: [
    "@nuxtjs/axios",
    "@nuxtjs/auth",
    "nuxt-i18n",
  ],
  i18n: {
    locales: [
      {
        code: "ar",
        file: "ar.json",
        name: "arabic",
      },
      {
        code: "en",
        file: "en.json",
        name: "english",
      },
    ],
    defaultLocale: "ar",
    strategy: "prefix_and_default",
    langDir: "@/plugins/i18n",
    lazy: true,
    useCookie: true,
    cookieKey: "language",
    detectBrowserLanguage: false,
    vueI18n: {
      fallbackLocale: "ar",
      silentTranslationWarn: true
    },
  },
  vuetify: {
    customVariables: ["assets/scss/_variables.scss"],
    treeShake: true,
    theme: {
      dark: false,
      light: true,
      themes: {
        light: theme.light,
      },
    },
  },
  router: {
    middleware: ["routeChange"],
  },
  axios: {
    baseURL: baseUrl(),
  },
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: "/login",
            method: "post",
            propertyName: "token",
          },
          logout: { url: "/logout", method: "delete" },
          user: {
            url: "show-account",
            method: "get",
          },
        },
      },
    },
    redirect: false,
    plugins: [{ src: "~/plugins/axios", ssr: true }],
  },
  plugins: [
    { src: "@/plugins/vuetify.js" },
    { src: "@/plugins/mixins.js"},
    { src: "@/plugins/vuelidate.js" },
    { src: "@/plugins/toasted.js", ssr: false },
  ],
  loadingIndicator: {
    name: "cube-grid",
    color: "#f00",
    background: "white",
  },
  build: {
    extractCSS: true,
    extend(config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          exclude: /(node_modules)/,
        });
      }
    },
  },
};

if (!Array.prototype.at) {
  Object.defineProperty(Array.prototype, 'at', {
    value: function (index) {
      index = Math.trunc(index) || 0;
      if (index < 0) index += this.length;
      if (index < 0 || index >= this.length) return undefined;
      return this[index];
    },
    writable: true,
    enumerable: false,
    configurable: true,
  });
}