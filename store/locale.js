
export const state= () => ({
  locale: ''
});

export const mutations = {
  SET_LOCALE(state, payload) {
    state.locale = payload;
  },
};

export const actions = {
  async setLocale({ commit }, payload) {
    commit("SET_LOCALE", payload)
    return Promise.resolve();
  },
};

export const getters = {
  getLocale({ locale }) {
    return locale;
  },
};



