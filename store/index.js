export const actions = {
  async nuxtServerInit({ dispatch }, context) {
    dispatch("locale/setLocale", context.app.i18n.locale);
  }

};
