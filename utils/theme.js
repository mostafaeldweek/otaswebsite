const theme = {
    light: {
        primary: "#0f6ffd",
        'dark-primary': "#84b4fb",
        'light-primary': "#e2ebf9",
        secondary: "#99cb3b",
        'dark-secondary': "#cce59d",
        'light-secondary': "#f4f9eb",
        erorr: '#ef1e1e',
        info: '#0cac9d',
        warning: '#0cac9d',
        success: "#5ab805"
    }
}

export default theme

